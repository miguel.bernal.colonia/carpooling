package com.pragma.powerup.carpooling.application.command;

import lombok.Data;

@Data
public class UserCommand {
  private String name;
  private String lastname;
  private String phoneNumber;
  private String address;
  private String email;
  private String password;
}
