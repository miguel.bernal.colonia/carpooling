package com.pragma.powerup.carpooling.application.factory;

import com.pragma.powerup.carpooling.application.command.UserCommand;
import com.pragma.powerup.carpooling.domain.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserFactory {

  public User mapUserCommandToUser(UserCommand userCommand) {
    return User.builder()
        .name(userCommand.getName())
        .lastname(userCommand.getLastname())
        .phoneNumber(userCommand.getPhoneNumber())
        .address(userCommand.getAddress())
        .email(userCommand.getEmail())
        .password(userCommand.getPassword())
        .build();
  }
}
