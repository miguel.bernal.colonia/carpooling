package com.pragma.powerup.carpooling.application.handler;

import com.pragma.powerup.carpooling.domain.model.UserDto;
import com.pragma.powerup.carpooling.domain.service.RetrieveUserByEmailService;
import org.springframework.stereotype.Component;

@Component
public class RetrieveUserByEmailHandler {

  private final RetrieveUserByEmailService retrieveUserByEmailService;

  public RetrieveUserByEmailHandler(RetrieveUserByEmailService retrieveUserByEmailService) {
    this.retrieveUserByEmailService = retrieveUserByEmailService;
  }

  private RetrieveUserByEmailService getRetrieveUserByEmailService() {
    return retrieveUserByEmailService;
  }

  public UserDto execute(final String email) {
    return getRetrieveUserByEmailService().execute(email);
  }
}
