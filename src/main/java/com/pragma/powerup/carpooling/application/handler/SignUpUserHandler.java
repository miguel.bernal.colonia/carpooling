package com.pragma.powerup.carpooling.application.handler;

import com.pragma.powerup.carpooling.application.command.UserCommand;
import com.pragma.powerup.carpooling.application.factory.UserFactory;
import com.pragma.powerup.carpooling.domain.model.User;
import com.pragma.powerup.carpooling.domain.service.SignUpUserService;
import org.springframework.stereotype.Component;

@Component
public class SignUpUserHandler {

  private final UserFactory userFactory;
  private final SignUpUserService signUpUserService;

  public SignUpUserHandler(
      final UserFactory userFactory, final SignUpUserService signUpUserService) {
    this.signUpUserService = signUpUserService;
    this.userFactory = userFactory;
  }

  private SignUpUserService getSignUpUserService() {
    return signUpUserService;
  }

  public UserFactory getUserFactory() {
    return userFactory;
  }

  public void execute(final UserCommand userCommand) {
    final User user = getUserFactory().mapUserCommandToUser(userCommand);
    getSignUpUserService().execute(user);
  }
}
