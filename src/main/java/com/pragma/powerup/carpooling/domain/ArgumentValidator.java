package com.pragma.powerup.carpooling.domain;

import com.pragma.powerup.carpooling.domain.exception.ArgumentValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgumentValidator {
  private ArgumentValidator() {}

  public static void validateRequired(Object value, String message) {
    if (value == null) {
      throw new ArgumentValidatorException(message);
    }
  }

  public static void validateMaxLength(String value, int length, String message) {
    if (value.length() > length) {
      throw new ArgumentValidatorException(message);
    }
  }

  public static void validateMinLength(String value, int length, String message) {
    if (value.length() < length) {
      throw new ArgumentValidatorException(message);
    }
  }

  public static void validateRegex(String value, String regex, String message) {
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(value);

    if (!matcher.matches()) {
      throw new ArgumentValidatorException(message);
    }
  }
}
