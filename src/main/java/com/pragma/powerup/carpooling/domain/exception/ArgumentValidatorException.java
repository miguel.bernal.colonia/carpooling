package com.pragma.powerup.carpooling.domain.exception;

public class ArgumentValidatorException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ArgumentValidatorException() {
    super();
  }

  public ArgumentValidatorException(String message) {
    super(message);
  }
}
