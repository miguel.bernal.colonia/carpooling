package com.pragma.powerup.carpooling.domain.model;

import static com.pragma.powerup.carpooling.domain.ArgumentValidator.validateRegex;
import static com.pragma.powerup.carpooling.domain.ArgumentValidator.validateRequired;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class User {

  public static final String NAME_MUST_BE_REQUIRED = "name is required";
  public static final String LASTNAME_MUST_BE_REQUIRED = "lastname is required";
  public static final String PHONE_NUMBER_MUST_BE_REQUIRED = "phone number is required";
  public static final String ADDRESS_MUST_BE_REQUIRED = "address is required";
  public static final String EMAIL_MUST_BE_REQUIRED = "email is required";
  public static final String PASSWORD_MUST_BE_REQUIRED = "password is required";

  public static final String PASSWORD_INVALID_FORMAT_MESSAGE = "invalid email structure";
  public static final String EMAIL_INVALID_FORMAT_MESSAGE = "invalid password structure";
  private static final String EMAIL_REGEX = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
  private static final String PASSWORD_REGEX =
      "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,15}$";

  private final String name;
  private final String lastname;
  private final String phoneNumber;
  private final String address;
  private final String email;
  private final String password;

  public User(
      String name,
      String lastname,
      String phoneNumber,
      String address,
      String email,
      String password) {

    validateRequired(name, NAME_MUST_BE_REQUIRED);
    validateRequired(lastname, LASTNAME_MUST_BE_REQUIRED);
    validateRequired(phoneNumber, PHONE_NUMBER_MUST_BE_REQUIRED);
    validateRequired(address, ADDRESS_MUST_BE_REQUIRED);
    validateRequired(email, EMAIL_MUST_BE_REQUIRED);
    validateRequired(password, PASSWORD_MUST_BE_REQUIRED);

    validateRegex(password, PASSWORD_REGEX, PASSWORD_INVALID_FORMAT_MESSAGE);
    validateRegex(email, EMAIL_REGEX, EMAIL_INVALID_FORMAT_MESSAGE);

    this.name = name;
    this.lastname = lastname;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.email = email;
    this.password = password;
  }

  // Cada servicio deberá tener sus pruebas unitarias.

  // Se tendrá en cuenta el uso de las buenas prácticas en las implementaciones.

  // Se requiere que se utilice una base de datos H2 para este primer entregable.

}
