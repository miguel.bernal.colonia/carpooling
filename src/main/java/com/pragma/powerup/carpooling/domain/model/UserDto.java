package com.pragma.powerup.carpooling.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {
  private String name;
  private String lastname;
  private String phoneNumber;
  private String address;
  private String email;
}
