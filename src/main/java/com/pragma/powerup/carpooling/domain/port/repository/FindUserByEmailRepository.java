package com.pragma.powerup.carpooling.domain.port.repository;

import com.pragma.powerup.carpooling.domain.model.UserDto;
import java.util.Optional;

public interface FindUserByEmailRepository {

  Optional<UserDto> findByEmail(final String email);
}
