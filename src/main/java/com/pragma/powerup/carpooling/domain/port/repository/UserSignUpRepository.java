package com.pragma.powerup.carpooling.domain.port.repository;

import com.pragma.powerup.carpooling.domain.model.User;

public interface UserSignUpRepository {

  void save(final User user);
}
