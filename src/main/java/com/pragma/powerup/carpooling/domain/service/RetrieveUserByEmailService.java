package com.pragma.powerup.carpooling.domain.service;

import com.pragma.powerup.carpooling.domain.exception.UserNotFoundException;
import com.pragma.powerup.carpooling.domain.model.UserDto;
import com.pragma.powerup.carpooling.domain.port.repository.FindUserByEmailRepository;
import org.springframework.stereotype.Service;

@Service
public class RetrieveUserByEmailService {
  private final FindUserByEmailRepository findUserByEmailRepository;

  public RetrieveUserByEmailService(final FindUserByEmailRepository findUserByEmailRepository) {
    this.findUserByEmailRepository = findUserByEmailRepository;
  }

  private FindUserByEmailRepository getFindUserByEmailRepository() {
    return findUserByEmailRepository;
  }

  public UserDto execute(final String email) {
    return getFindUserByEmailRepository()
        .findByEmail(email)
        .orElseThrow(UserNotFoundException::new);
  }
}
