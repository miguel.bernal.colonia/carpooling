package com.pragma.powerup.carpooling.domain.service;

import com.pragma.powerup.carpooling.domain.model.User;
import com.pragma.powerup.carpooling.domain.port.repository.UserSignUpRepository;
import org.springframework.stereotype.Service;

@Service
public class SignUpUserService {

  private final UserSignUpRepository userSignUpRepository;

  public SignUpUserService(final UserSignUpRepository userSignUpRepository) {
    this.userSignUpRepository = userSignUpRepository;
  }

  private UserSignUpRepository getUserSignUpRepository() {
    return userSignUpRepository;
  }

  public void execute(final User user) {
    getUserSignUpRepository().save(user);
  }
}
