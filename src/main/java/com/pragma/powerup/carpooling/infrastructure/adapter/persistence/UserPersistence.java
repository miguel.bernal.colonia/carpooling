package com.pragma.powerup.carpooling.infrastructure.adapter.persistence;

import com.pragma.powerup.carpooling.infrastructure.adapter.persistence.entity.UserEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPersistence extends JpaRepository<UserEntity, Integer> {

  Optional<UserEntity> findByEmail(final String email);
}
