package com.pragma.powerup.carpooling.infrastructure.adapter.port.repository;

import com.pragma.powerup.carpooling.domain.model.User;
import com.pragma.powerup.carpooling.domain.model.UserDto;
import com.pragma.powerup.carpooling.domain.port.repository.FindUserByEmailRepository;
import com.pragma.powerup.carpooling.domain.port.repository.UserSignUpRepository;
import com.pragma.powerup.carpooling.infrastructure.adapter.persistence.UserPersistence;
import com.pragma.powerup.carpooling.infrastructure.adapter.persistence.entity.UserEntity;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class MemoryUserSignUpRepository implements UserSignUpRepository, FindUserByEmailRepository {

  private final UserPersistence userPersistence;

  public MemoryUserSignUpRepository(final UserPersistence userPersistence) {
    this.userPersistence = userPersistence;
  }

  public UserPersistence getUserPersistence() {
    return userPersistence;
  }

  @Override
  public void save(final User user) {
    UserEntity userEntity = mapUserToUserEntity(user);
    getUserPersistence().save(userEntity);
  }

  @Override
  public Optional<UserDto> findByEmail(String email) {
    return getUserPersistence().findByEmail(email).flatMap(this::mapUserEntityToUserDto);
  }

  private UserEntity mapUserToUserEntity(final User user) {
    return UserEntity.builder()
        .name(user.getName())
        .lastname(user.getLastname())
        .phoneNumber(user.getPhoneNumber())
        .address(user.getAddress())
        .email(user.getEmail())
        .password(user.getPassword())
        .build();
  }

  private Optional<UserDto> mapUserEntityToUserDto(UserEntity userEntity) {
    UserDto userDto =
        UserDto.builder()
            .name(userEntity.getName())
            .lastname(userEntity.getLastname())
            .phoneNumber(userEntity.getPhoneNumber())
            .address(userEntity.getAddress())
            .email(userEntity.getEmail())
            .build();

    return Optional.of(userDto);
  }
}
