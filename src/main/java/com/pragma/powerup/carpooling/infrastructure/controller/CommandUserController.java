package com.pragma.powerup.carpooling.infrastructure.controller;

import com.pragma.powerup.carpooling.application.command.UserCommand;
import com.pragma.powerup.carpooling.application.handler.SignUpUserHandler;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class CommandUserController {

  private final SignUpUserHandler signUpUserHandler;

  public CommandUserController(final SignUpUserHandler signUpUserHandler) {
    this.signUpUserHandler = signUpUserHandler;
  }

  private SignUpUserHandler getSignUpUserHandler() {
    return signUpUserHandler;
  }

  @PostMapping
  public void signUp(@RequestBody final UserCommand userCommand) {
    getSignUpUserHandler().execute(userCommand);
  }
}
