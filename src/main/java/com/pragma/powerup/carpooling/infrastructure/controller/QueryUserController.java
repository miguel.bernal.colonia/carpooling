package com.pragma.powerup.carpooling.infrastructure.controller;

import com.pragma.powerup.carpooling.application.handler.RetrieveUserByEmailHandler;
import com.pragma.powerup.carpooling.domain.model.UserDto;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class QueryUserController {

  private final RetrieveUserByEmailHandler retrieveUserByEmailHandler;

  public QueryUserController(final RetrieveUserByEmailHandler retrieveUserByEmailHandler) {
    this.retrieveUserByEmailHandler = retrieveUserByEmailHandler;
  }

  public RetrieveUserByEmailHandler getRetrieveUserByEmailHandler() {
    return retrieveUserByEmailHandler;
  }

  @GetMapping
  public UserDto retrieveById(@RequestParam final String email) {
    return getRetrieveUserByEmailHandler().execute(email);
  }
}
