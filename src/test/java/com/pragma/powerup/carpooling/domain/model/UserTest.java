package com.pragma.powerup.carpooling.domain.model;

import com.pragma.powerup.carpooling.domain.exception.ArgumentValidatorException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserTest {

  @Test()
  void userPasswordShouldSatisfyConditions() {

    String password = "Prueba1*";
    User user =
        User.builder()
            .name("pepito")
            .lastname("perez")
            .phoneNumber("3219033398")
            .address("Carrera 1 # 2 - 3")
            .email("email@gmail.com")
            .password(password)
            .build();

    Assertions.assertEquals(password, user.getPassword());
  }

  @Test
  void userWithoutName() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.NAME_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test
  void userWithoutLastName() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.LASTNAME_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test
  void userWithoutPhoneNumber() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.PHONE_NUMBER_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test
  void userWithoutAddress() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .email("email@gmail.com")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.ADDRESS_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test
  void userWithoutEmail() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.EMAIL_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test
  void userWithoutPassword() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_MUST_BE_REQUIRED, thrown.getMessage());
  }

  @Test()
  void userPasswordShouldNotHaveLessThan8Characters() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Prueba1")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void userPasswordShouldNotHaveMoreThan15Characters() {
    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Prueba123*_123rf")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void userPasswordShouldHaveAtLeastOneUpperCaseCharacter() {
    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("prueba1*")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void passwordShouldHaveAtLeastOneLowerCaseCharacter() {
    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("PRUEBA1*")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void passwordShouldHaveAtLeastOneNumberCharacter() {
    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Pruebas*")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void passwordShouldHaveAtLeastOneSpecialCharacter() {
    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("email@gmail.com")
                  .password("Pruebass")
                  .build();
            });

    Assertions.assertEquals(User.PASSWORD_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }

  @Test()
  void userEmailShouldHaveValidStructure() {

    String email = "email@gmail.com";
    User user =
        User.builder()
            .name("pepito")
            .lastname("perez")
            .phoneNumber("3219033398")
            .address("Carrera 1 # 2 - 3")
            .email(email)
            .password("Prueba1*")
            .build();

    Assertions.assertEquals(email, user.getEmail());
  }

  @Test()
  void userEmailWithoutValidStructure() {

    ArgumentValidatorException thrown =
        Assertions.assertThrows(
            ArgumentValidatorException.class,
            () -> {
              User.builder()
                  .name("pepito")
                  .lastname("perez")
                  .phoneNumber("3219033398")
                  .address("Carrera 1 # 2 - 3")
                  .email("emailgmail.com")
                  .password("Prueba1*")
                  .build();
            });

    Assertions.assertEquals(User.EMAIL_INVALID_FORMAT_MESSAGE, thrown.getMessage());
  }
}
