package com.pragma.powerup.carpooling.domain.service;

import static org.mockito.Mockito.when;

import com.pragma.powerup.carpooling.domain.exception.UserNotFoundException;
import com.pragma.powerup.carpooling.domain.model.UserDto;
import com.pragma.powerup.carpooling.domain.port.repository.FindUserByEmailRepository;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RetrieveUserByEmailServiceTest {

  @Mock private FindUserByEmailRepository findUserByEmailRepository;

  @InjectMocks private RetrieveUserByEmailService retrieveUserByEmailService;

  @Test
  void shouldRetrieveAnUser() {
    // Arrange

    String email = "email@gmail.com";

    UserDto userDto =
        UserDto.builder()
            .name("pepito")
            .lastname("perez")
            .phoneNumber("3219033398")
            .address("Carrera 1 # 2 - 3")
            .email(email)
            .build();

    Optional<UserDto> optional = Optional.of(userDto);

    when(findUserByEmailRepository.findByEmail(email)).thenReturn(optional);

    // Act
    UserDto expectedUser = retrieveUserByEmailService.execute(email);

    // Assert
    Assertions.assertEquals(optional.get(), expectedUser);
  }

  @Test
  void shouldThrowAnExceptionUser() {

    // Arrange
    String email = "email@gmail.com";
    Optional<UserDto> optional = Optional.empty();

    when(findUserByEmailRepository.findByEmail(email)).thenReturn(optional);

    // Act - Assert
    Assertions.assertThrows(
        UserNotFoundException.class,
        () -> {
          retrieveUserByEmailService.execute(email);
        });
  }
}
