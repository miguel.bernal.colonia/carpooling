package com.pragma.powerup.carpooling.domain.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import com.pragma.powerup.carpooling.domain.model.User;
import com.pragma.powerup.carpooling.domain.port.repository.UserSignUpRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SignUpUserServiceTest {

  @Mock private UserSignUpRepository userSignUpRepository;

  @InjectMocks private SignUpUserService signUpUserService;

  @Test
  void shouldSaveUser() {
    // Arrange

    User user =
        User.builder()
            .name("pepito")
            .lastname("perez")
            .phoneNumber("3219033398")
            .address("Carrera 1 # 2 - 3")
            .email("email@gmail.com")
            .password("Prueba1*")
            .build();

    doNothing().when(userSignUpRepository).save(any(User.class));

    // Act
    this.signUpUserService.execute(user);

    // Assert
    verify(userSignUpRepository).save(any(User.class));
  }
}
